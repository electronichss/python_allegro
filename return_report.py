import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
from scipy import stats
from pathlib import Path, PureWindowsPath
import glob, os, csv, shutil, sys


tests = []
hot_tests = [
##'Init Qvo',
##'Trimmed Qvo',
##'Hot Qvo',
##'Hot Qvo trimmed',
##'Room Qvo',
##'QVO RAT (5V_5p15V)',
##'QVO RAT (5V_4p85V)',
##'Final Room Qvo @ Hot',
##'Final Hot Qvo',
##'UVLO Fall',
##'UVLO Rise',
##'POR Falling',
##'TD Step Size',
##'Final Sense @ 5V',
##'Final Hot Sense',
##'Final Hot Sense Error',
'SNS RAT (5V_4p85V)',
'Max Gain Output 3p8V Oscillation']




cold_tests = [
##'Room Qvo',
##'Cold Qvo',
##'Cold Qvo trimmed',
##'QVO RAT (5V_5p15V)',
##'QVO RAT (5V_4p85V)',
##'Final Room Qvo @ Cold',
##'Final Cold Qvo',
##'UVLO Fall',
##'UVLO Rise',
##'POR Falling',
##'TD Step Size',
##'Room Sense',
##'Final Room Sense @ Cold',
##'Final Cold Sense Error',
'SNS RAT (5V_4p85V)',
'Max Gain Output 3p8V Oscillation']


path = r'C:\Users\cbyers\Monday_Morning_Quarterback\1364\comp\45th'


infile_cold = '6650_infile.csv'
infile_hot = '6550_infile.csv'


return_file_cold = '6650_return.csv'
return_file_hot = '6550_return.csv'


temp = 0 #1 for hot, 0 for cold
topsite = 1

search = 0
report = 1

save_fig = 0



if temp:
    infile = infile_hot
    tests = hot_tests
    return_file = return_file_hot
    fileappend = 'hot'

else:
    infile = infile_cold
    tests = cold_tests
    return_file = return_file_cold
    fileappend = 'cold'


ef = [0]*len(tests)
HighL = [0]*len(tests)
LowL = [0]*len(tests)
mu = [0]*len(tests)
sigma = [0]*len(tests)
threeS = [0]*len(tests)


os.chdir(path)

if search:
    for i in range(len(tests)):
        f_tests = tests[i]
        f_tests=f_tests.split(',') #convert input test to list instead of string, some pandas requirement
        print(i, '/', len(tests))
        #try:
        df = pd.read_csv(return_file,skiprows =[1, 2, 3, 4, 5],error_bad_lines=False, usecols = f_tests, dtype='unicode')
        #except:
        #    print("test missing: ",tests[i])



if report:


    for i in range(len(tests)):

        jtests = tests[i]
        ftests = ['SITE', jtests]

        #added seaborn options, dunno what they do
        sns.set(style="white", palette="muted", color_codes=True)

        df = pd.read_csv(infile,skiprows =[1, 2, 3, 4, 5],error_bad_lines=False, usecols = ftests, dtype='unicode')
        df_return = pd.read_csv(return_file,skiprows =[1, 2, 3, 4, 5],error_bad_lines=False, usecols = ftests, dtype='unicode')
        d = pd.read_csv(return_file,skiprows =lambda x: x not in [0,1,2,3,4,5],error_bad_lines=False, na_filter=True, usecols = ftests,dtype='unicode')#for units


        #return dataframe to numeric
        dff_return = pd.to_numeric(df_return[ftests[i]], errors='coerce')
        site_dff = pd.to_numeric(df[ftests[0]],errors='coerce')



        #lot dataframe to numeric
        dff = pd.to_numeric(df[ftests[1]], errors='coerce')
        dff_filtered = dff[dff.between(dff.quantile(.0001), dff.quantile(.9999))] #remove outliers


        #pull return data by site
        if(topsite):
            ef[i] = float(df_return[ftests[1]].loc[0])
        else:
            ef[i] = float(df_return[ftests[1]].loc[1])


        #calculation
        HighL[i] = float(d[ftests[1]].iloc[3])
        LowL[i] =float(d[ftests[1]].iloc[4])



        mu[i] = dff_filtered.mean(axis=0, skipna = True)
        sigma[i] = dff.std(axis=0, skipna = True)
        threeS[i] = 3*sigma

  
        textstr = '\n'.join((r'$\mu=%.4f$' % (mu[i]),r'$\sigma=%.3f$' % (sigma[i]), r'ss=%.0f' % (len(dff_filtered)), r'return unit:=%.5f' %(ef[i]),r'High Limit:=%.5f'%(HighL[i]),r'Low Limit:=%.5f'%(LowL[i])))

        axis_high = float(HighL[i])*1.005
        axis_low = float(LowL[i])*.991
        
        #sns.distplot(dff_filtered, kde=True, color="b", fit=stats.gamma)
        fig, ax = plt.subplots()

        #plt.axvline(HighL[i], color='m', linestyle='solid', linewidth=1) #dashed line
        #plt.axvline(LowL[i], color='m', linestyle='solid', linewidth=1) #dashed line
        
        ax.hist(dff_filtered,100)#, range=[axis_low, axis_high])
        props = dict(boxstyle='round', facecolor='silver', alpha=0.5)
        ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=9,verticalalignment='top', bbox=props)
        
        plt.title(fileappend+'_'+tests[i])
        plt.axvline(ef[i], color='black', linestyle='dashed', linewidth=1) #dashed line


        if(save_fig):
            plt.savefig(fileappend+'_'+str(i)+tests[i])

        #can save images to file here
        plt.show()
        

        

        

